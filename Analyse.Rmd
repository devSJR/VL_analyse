---
title: "Analyse"
author: "Stefan Rödiger"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
    pdf_document:
      fig_caption: yes
      highlight: kate
      number_sections: true
      toc: yes
urlcolor: blue
linkcolor: red
csl: elsevier-vancouver.csl
bibliography: lit.bib
---

\pagebreak

```{r, echo=FALSE, include=FALSE}
# Defaults to suppress warnings and messages
# Figure height and width are set to 7 inches
# See more options by running str(knitr::opts_chunk$get())

knitr::opts_chunk$set(
  echo = FALSE, error = FALSE, warnings = FALSE, message = FALSE,
  collapse = FALSE, fig.width = 10, fig.height = 5
)

library("countfitteR")
library("knitr")
library("beeswarm")
library("data.table")
library("MASS")
library("skimr")
```

# Methods

The foci count data were processed in RKWard [@rodiger_rkward:_2012] with the
programming language R [@R_base]. The countfitteR [@Chilimoniuk2021] package
was used to determine the distributions and mean values.

# Data import

\tiny

```{r, raw_data, echo = FALSE, eval = TRUE, fig.show = "asis", include = TRUE, message = FALSE, results = "asis", warning = FALSE}
data_Hep2 <- read.csv("HEp-2_.csv", sep = ",")
data_U2OS <- read.csv("U2OS_.csv", sep = ",")
data_LoVo <- read.csv("LoVo_.csv", sep = ",")

# Check dimension of the data

data_Hep2 |> dim()
data_LoVo |> dim() # Bei data_LoVo fehlt spalte 'green'
data_U2OS |> dim()
```

# Aggregation of the data

```{r}
dat <- rbind(
  data.frame(
    cellline = rep("Hep2", nrow(data_Hep2)),
    data_Hep2[, c("Group", "replicate", "yH2AX")]
  ),
  data.frame(
    cellline = rep("LoVo", nrow(data_LoVo)),
    data_LoVo[, c("Group", "replicate", "yH2AX")]
  ),
  data.frame(
    cellline = rep("U2OS", nrow(data_U2OS)),
    data_U2OS[, c("Group", "replicate", "yH2AX")]
  )
) |> as.data.table()
```


# Deprecated way of aggregation

```{r}
|# echo: TRUE
|# eval: FALSE

data_list <- c("data_Hep2", "data_U2OS", "data_LoVo") |> mget()

lapply(1:length(data_list), function(i) {
  data_list[i] |>
    data.frame() |>
    head() |>
    kable()
})

lapply(1:length(data_list), function(i) {
  data_list[i] |>
    data.frame() |>
    summary() |>
    skim()
})
```

\normalsize

# Data analysis

```{r, chunk-label, echo = FALSE, eval = TRUE, fig.show = "asis", include = TRUE, message = FALSE, results = "asis", warning = FALSE}
fit <- glm.nb(data = data_Hep2, yH2AX ~ Group)
summary(fit)
```


```{r, helperfunction, echo = FALSE, eval = FALSE, fig.show = "asis", include = TRUE, message = FALSE, results = "asis", warning = FALSE}

DT <- data_list |> as.data.table()



data_Hep2.DT <- data_Hep2 |> as.data.table()

data_Hep2.DT[, yH2AX, by = .(Group, replicate)]


names <- c("data_Hep2.Group")
DT[, .(Median = median(data_Hep2.yH2AX), Mean = mean(data_Hep2.yH2AX), Std = sd(data_Hep2.yH2AX)), by = names] |> kable()
```


```{r, init, echo = FALSE, eval = TRUE, fig.show = "asis", include = TRUE, message = FALSE, results = "asis", warning = FALSE}
par(mfrow = c(1, 3))
stripchart(data_Hep2[["yH2AX"]] ~ data_Hep2[["Group"]],
  xlab = "Condition",
  ylab = "Counts / Cell", main = "Hep-2", vertical = TRUE, method = "jitter",
  pch = 19, col = data_Hep2[["replicate"]]
)
stripchart(data_U2OS[["yH2AX"]] ~ data_U2OS[["Group"]],
  xlab = "Condition",
  ylab = "Counts / Cell", main = "U2OS", vertical = TRUE, method = "jitter",
  pch = 19, col = data_U2OS[["replicate"]]
)
stripchart(data_LoVo[["yH2AX"]] ~ data_LoVo[["Group"]],
  xlab = "Condition",
  ylab = "Counts / Cell", main = "LoVo", vertical = TRUE, method = "jitter",
  pch = 19, col = data_LoVo[["replicate"]]
)
```

# Analysis

The foci count data were processed in RKWard with the programming language R.
The countfitteR package was used to determine the distributions and mean values.

In detail, the averages (lambda) were determined per cell line and group and
replicate by first adjusting the distribution functions Poisson (pois), Negativ
Binomial (nb), Zero Inflated Possion (zip) and Zero Inflated Negative Binomial
(zinb). The optimal distribution was selected by the Bayesian's An Information
Criterion (BIC).

The average value and the standard deviation were determined from the individual
values of the replicates per cell line and group. With these values the diagrams
were created and the statistical comparison was carried out.

```{r}
counter <- function(data) {
  count_data <- data.frame(data)
  yH2AX_model <- suppressWarnings(fit_counts(count_data, model = "all") |> select_model())

  if (yH2AX_model[["chosen_model"]] == "Poisson") res_model <- "pois"
  if (yH2AX_model[["chosen_model"]] == "ZIP") res_model <- "zip"
  if (yH2AX_model[["chosen_model"]] == "ZINB") res_model <- "zinb"
  if (yH2AX_model[["chosen_model"]] == "NB") res_model <- "nb"

  res_fit_counts <- fit_counts(count_data, model = res_model) |> summary_fitlist()

  data.frame(
    model = res_model,
    lambda = res_fit_counts[["lambda"]],
    lower = res_fit_counts[["lower"]],
    upper = res_fit_counts[["upper"]]
  )
}

res <- dat[, .(
  N = length(yH2AX),
  mean_yH2AX = mean(yH2AX),
  sd_yH2AX = sd(yH2AX),
  model = as.factor(counter(yH2AX)[["model"]]),
  lambda = counter(yH2AX)[["lambda"]],
  lower = counter(yH2AX)[["lower"]],
  upper = counter(yH2AX)[["upper"]]
),
by = .(cellline, Group, replicate)
]
```

\small
\newpage

```{r}
res |>
  as.data.frame() |>
  kable()
```

\normalsize

```{r}
res_summary <- res[, .(
  N = length(lambda),
  mean_yH2AX = mean(lambda),
  sd_yH2AX = sd(lambda)
),
by = .(cellline, Group)
]

res_summary[, `:=` (order = rep(c(3,2,1), 3))]

setorder(res_summary, cols = "cellline", "order")
```


# Data for poster

```{r}
res_summary
```

```{r, fig.height=5, fig.width=7}
stat_Hep2 <- TukeyHSD(aov(res[cellline == "Hep2", mean_yH2AX, ] ~ res[cellline == "Hep2", Group, ]))
stat_LoVo <- TukeyHSD(aov(res[cellline == "LoVo", mean_yH2AX, ] ~ res[cellline == "LoVo", Group, ]))
stat_U2OS <- TukeyHSD(aov(res[cellline == "U2OS", mean_yH2AX, ] ~ res[cellline == "U2OS", Group, ]))

par(las = 1)
plt <- barplot(res_summary[, mean_yH2AX, ],
ylab = "foci/cell", ylim = c(-1,6),
col = c(rep(c(2,3,4), 3)), axes = FALSE,
border = NA)
text(plt[, 1], rep(-0.5, nrow(plt)), labels = res_summary[, Group, ])
axis(2, at = 0:5)
arrows(plt[, 1], res_summary[, mean_yH2AX, ] - res_summary[, sd_yH2AX, ],
        plt[, 1], res_summary[, mean_yH2AX, ] + res_summary[, sd_yH2AX, ],
        angle = 90, code = 0, lwd = 2)
abline(v= c(mean(plt[c(3,4)]), mean(plt[c(3,4) + 3])), col = "lightgrey")
mtext('Hep-2', side=1, line=-0.5, at=plt[2, ])
mtext('LoVo', side=1, line=-0.5, at=plt[5, ])
mtext('U2OS', side=1, line=-0.5, at=plt[8, ])

stat_Hep2[[1]][, "p adj"] < 0.05
stat_LoVo[[1]][, "p adj"] < 0.05
stat_U2OS[[1]][, "p adj"] < 0.05

# Hep-2
xx1 <- 4.5
arrows(plt[1, ], xx1, plt[2, ], xx1, code = 0)

# LoVo
xx1 <- 2.5
xx2 <- 3
arrows(plt[5, ], xx1, plt[6, ], xx1, code = 0)
arrows(plt[4, ], xx2, plt[6, ], xx2, code = 0)

# U2OS
xx1 <- 4.7
xx2 <- 5.2
arrows(plt[8, ], xx1, plt[9, ], xx1, code = 0)
arrows(plt[7, ], xx2, plt[9, ], xx2, code = 0)
```

# Statistics

## Hep-2

```{r}
stat_Hep2
```

rmarkdown::

## LoVo

```{r}
stat_LoVo
```

## U2OS

```{r}
stat_U2OS
```

# Analysis from

> Fwd: Manuelle Zählung HEp-2 OE
>￼
> From:	Victoria Liedtke <liedtvic@b-tu.de>
> To:	Stefan Rödiger <Stefan.Roediger@b-tu.de>
> Date:	23.01.23 17:21

# References

